import React from "react";
import { BrowserRouter, Switch} from "react-router-dom";
import "./App.css";
import Home from "./components/home";
import PublicRoute from "./components/Utils/PublicRoute";

function App() {
  return (
        <BrowserRouter>
          <Switch>
            <PublicRoute exact path="/" component={Home} />
          </Switch>
        </BrowserRouter>
  );
}

export default App;

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";


const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CarKeyspecificationcol1(props) {

  console.log("car specs-->"+props.data.brakes);

  const classes = useStyles();

  return (
    <div className={classes.root}>
          <List>
            <ListItem>
              <ListItemText primary="Door" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.doors} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Seats" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.seats} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            
            <ListItem>
              <ListItemText primary="Parts List" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.partslist} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            
            <ListItem>
              <ListItemText primary="Engine Comp Ratio" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.engineCompRatio} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
          <ListItem>
              <ListItemText primary="Fuel Supl Details" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.fuelSuplDetails} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Torque_rpm" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.torque_rpm}/>
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Engine Speed120" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.engineSpeed120} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Transm Details" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.transmDetails} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="AccelDetails" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.accelDetails} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Fuel Capacity" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.fuelCapacity} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="OTaccelDetails" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.OTaccelDetails} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="SpeedCalibration" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.speedCalibration} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="fuelDetails" />
              <ListItemSecondaryAction>
                <ListItemText primary={props.data.fuelDetails} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />  
          </List>
    </div>
  );
}

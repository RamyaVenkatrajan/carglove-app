import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CarKeyspecificationcol2(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
          <List>
            <ListItem>
              <ListItemText primary="Price" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.price} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Gears" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.gears} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Brakes" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.brakes} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Engine Bore Stroke" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs"  primary={props.data.engineBoreStroke} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            
            <ListItem>
              <ListItemText primary="Valve Gear" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.valveGear} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemText primary="Power_rpm" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.power_rpm} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Wheels Tyres" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.wheelsTyres} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Steering Details" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.steeringDetails} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Suspension" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.suspension} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Warranty" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.warranty} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
             <ListItem>
              <ListItemText primary="Vehicle Dimensions" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.vehicleDimensions} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider /> 
            <ListItem>
              <ListItemText primary="Features List" />
              <ListItemSecondaryAction>
                <ListItemText className="carModal-car-specs" primary={props.data.featuresList} />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider /> 
                    
          </List>
    </div>
  );
}

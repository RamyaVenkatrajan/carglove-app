import React, { useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Cardetailgallery from "./Cardetailgallery";
import CarKeyspecificationcol1 from "./CarKeyspecificationcol1";
import CarKeyspecificationcol2 from "./CarKeyspecificationcol2";
import CarKeyfeaturescol1 from "./CarKeyfeaturescol1";
import CarKeyfeaturescol2 from "./CarKeyfeaturescol2";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import "../../App.css";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    top: -5,
    color: "#ffffff",
    right: -5,
    padding: 0,
    position: "absolute",
    background: "#dc3545",
    width: 15,
    height: 15,
    "&:hover": {
      background: "#dc3545",
    },
  },
  title: {
    margin: 0,
    padding: theme.spacing(2),
    position: "relative",
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.title} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const useStyles = makeStyles((theme) => ({
  mdldetailsbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },
}));

export default function CustomizedDialogs(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [fullWidth] = React.useState(true);
  const [maxWidth] = React.useState("md");
  const [carDetailsValues, setcarDetailsValues] = React.useState([]);

  console.log("car details row values --->", props.data);
  useEffect(() => {
    fetchData();
  }, []);

  const id = props.data;
  const fetchData = () => {
    setLoading(true);
    fetch(`http://localhost:3090/v1/data/cardetails?id=` + id, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response.data);
        console.log(
          "model name --> " + response.data.carDetailsResultSet[0].modelName
        );
        setcarDetailsValues(response.data.carDetailsResultSet[0]);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  /* useEffect(() => {
    fetch(`http://localhost:3090/v1/data/cardetails`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response.data);
        console.log("model name --> "+response.data.carDetailsResultSet[0].modelName);
        setcarDetailsValues(response.data.carDetailsResultSet[0]);
        //setIsLoading(false);
      })
      .catch((error) => console.log(error));
  }); */

  return (
    <div>
      <Button
        variant="contained"
        size="small"
        color="primary"
        onClick={handleClickOpen}
        className={classes.mdldetailsbtns}
      >
        Details
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullWidth={fullWidth}
        maxWidth={maxWidth}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          style={{ textAlign: "center", fontSize: 18, color: "#4f4f4f" }}
        >
          Car Detail
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <Cardetailgallery />
            </Grid>

            <Grid item xs={12} sm={12} md={6} lg={6}>
              <List className="car-detailsTop">
                <ListItem>
                  <ListItemText primary="Model ID" />
                  <ListItemSecondaryAction>
                    <ListItemText primary={carDetailsValues.modelId} />
                  </ListItemSecondaryAction>
                </ListItem>

                <ListItem>
                  <ListItemText primary="Model Name" />
                  <ListItemSecondaryAction>
                    <ListItemText primary={carDetailsValues.modelName} />
                  </ListItemSecondaryAction>
                </ListItem>
              </List>
              <Typography
                gutterBottom
                style={{
                  fontSize: 18,
                  color: "#9c1233",
                  marginLeft: 16,
                  marginBottom: 10,
                }}
              >
                Description
              </Typography>
              <Typography
                gutterBottom
                style={{
                  fontSize: 14,
                  color: "#4f4f4f",
                  marginLeft: 16,
                }}
              >
                {carDetailsValues.description}
              </Typography>
            </Grid>
          </Grid>

          <Grid container>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <Typography
                style={{
                  fontSize: 18,
                  marginTop: 20,
                  color: "#9c1233",
                  paddingLeft: 16,
                }}
              >
                Key Specification
              </Typography>
              <CarKeyspecificationcol1 data={carDetailsValues} />
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <Typography
                style={{
                  fontSize: 18,
                  marginTop: 20,
                  color: "#4f4f4f",
                  paddingLeft: 16,
                  visibility: "hidden",
                }}
              >
                Key Specification
              </Typography>
              <CarKeyspecificationcol2 data={carDetailsValues} />
            </Grid>
          </Grid>

          <Grid container>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <Typography
                style={{
                  fontSize: 18,
                  marginTop: 20,
                  color: "#9c1233",
                  paddingLeft: 16,
                }}
              >
                Key Features
              </Typography>
              <CarKeyfeaturescol1 data={carDetailsValues} />
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <Typography
                style={{
                  fontSize: 18,
                  marginTop: 20,
                  color: "#4f4f4f",
                  paddingLeft: 16,
                  visibility: "hidden",
                }}
              >
                Key Features
              </Typography>
              <CarKeyfeaturescol2 data={carDetailsValues} />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} style={{ color: "#9c1233" }}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

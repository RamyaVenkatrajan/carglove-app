import React from "react";
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";

const images = [{
        original: "/static/images/avatar/preview.jpg",
        thumbnail: "/static/images/avatar/preview.jpg",
    },
    {
        original: "/static/images/avatar/preview.jpg",
        thumbnail: "/static/images/avatar/preview.jpg",
    },
];

class Cardetailgallery extends React.Component {
    render() {
        return <ImageGallery items = { images }
        showPlayButton={false}
        thumbnailPosition="left"
        showFullscreenButton={false}
        showNav={false}
        />;
    }
}
export default Cardetailgallery;
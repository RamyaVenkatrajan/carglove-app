import React, { forwardRef, useState } from "react";
import "./styles.css";
import Slider from "react-rangeslider";
import Chip from "@material-ui/core/Chip";
import Tooltip from "@material-ui/core/Tooltip";

export default forwardRef(({ data, classValue }, ref) => {
  const [volume, setVolume] = useState(50);
  const changeVolume = (ev) => {
    setVolume(20);
    //    ev.dataTransfer.getData("source");
  };

 /*  const verticalLabels = {
    0: "L",
    50: "M",
    100: "H",
  };
 */
  console.log("Volume after change-->" + volume);

  return (
    <div className={`item ${classValue}`} ref={ref}>
      <button className="closebutton" name="removeTask" onClick={changeVolume}>
        x
      </button>
      <Tooltip title={<h6 style={{ color: "white", fontSize: "12px", fontWeight: "normal", fontFamily: "sans-serif" }}>{data.description}</h6>} arrow placement="top">
        <span className="rangeSlider_values">{data.label}</span>
      </Tooltip>
      <Chip label={volume} className="badgevalue" />
      <Slider
        value={volume}
        orientation="vertical"
        //handleLabel={volume}
        //labels={verticalLabels}
        onChange={(value) => setVolume(value)}
      />
    </div>
  );
});

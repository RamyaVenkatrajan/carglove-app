import React, { forwardRef } from "react";
import "./styles.css";
//import "../../App.css";
import "react-rangeslider/lib/index.css";

export default forwardRef(({ children, heading }, ref) => {
  return (
    <div className="container1" ref={ref}>
      <div className={heading}>{children}</div>
    </div>
  );
});

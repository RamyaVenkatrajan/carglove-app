//react-import
import React, { useState, useEffect } from "react";
//files-import
import Sidebar from "./Sidebar";
import Maincontent from "./Maincontent";
//import SearchResults from "./SearchResults";

//styles-import
import Grid from "@material-ui/core/Grid";
import { makeStyles } from '@material-ui/styles';

//searchkit-import
import { gql } from "@apollo/client";
import { useSearchkitQuery } from "@searchkit/client";

const QUERY = gql`
query resultSet(
    $query: String
    $filters: [FiltersSet]
    $page: PageInput
    $sortBy: String
  ) {
    results(query: $query, filters: $filters) {
      summary {
        total
        appliedFilters {
          id
          identifier
          display
          label
          ... on DateRangeSelectedFilter {
            dateMin
            dateMax
          }
          ... on NumericRangeSelectedFilter {
            min
            max
          }
          ... on ValueSelectedFilter {
            value
          }
        }
        sortOptions {
          id
          label
        }
        query
      }
      hits(page: $page, sortBy: $sortBy) {
        page {
          total
          totalPages
          pageNumber
          from
          size
        }
        sortedBy
        items {
          id
          fields {
            modelId
            model_data{
              modelName
              price
              monthlyExp
              monthlyInstallment
              seats
              doors
              country{
                name
              }
              bodyStyle{
                name
              }
            }
          }
        }
      }
      facets {
        identifier
        type
        label
        display
        entries {
          id
          label
          count
        }
      }
    }
  }
  `;

const useStyles = makeStyles({
  spacing: {
    paddingRight: 10,
    paddingLeft: 10,
    marginTop: 20,
  },
  m20: {},
});

const Container = (props) => {
  const classes = useStyles(props);
  console.log("print-query-container", QUERY);
  const { data, loading } = useSearchkitQuery(QUERY);
  const [searchRes, setSearchRes] = useState([]);
  const [carsData, setCarsData] = useState([]);
  useEffect(() => {
    if (data) {
      console.log("print-inside-usereffect-container:", data);
      setCarsData(data);
      setSearchRes(data.results.hits.items);
    }
  }, [data]);
  console.log("print-data-container :", data);
  console.log("print-carsdata-container :", carsData);
  if (!data) {
    return <h1>loading...</h1>;
  }
  return (
    <div>
      <div>
        <Grid container className="filtercontainer">
          <Grid item xs={12} sm={12} md={3} lg={3} className={classes.spacing}>
            <Sidebar data={data} testData="trst" loading={loading} />
          </Grid>

          <Grid item xs={12} sm={12} md={9} lg={9} className={classes.spacing} >
            <Maincontent />
            {/* <SearchResults test={searchRes} /> */}
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default Container;

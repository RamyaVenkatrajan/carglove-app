import React, { useState, useEffect } from "react";
import { useSearchkit } from "@searchkit/client";

//styles-import
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Rangeslider from "./Rangeslider";
import Avatar from "@material-ui/core/Avatar";
import Checkbox from "@material-ui/core/Checkbox";

//theme
const outerTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#EA001E !important",
    },
    secondary: {
      main: "#EA001E !important",
    },
  },
});

//styles
const useStyles = (theme) => ({
  accorheading: {
    fontSize: 16,
    color: "#4f4f4f",
  },
  sidebaraccordion: {
    marginTop: "0 !important",
    marginBottom: "0 !important",
    "& .MuiAccordionSummary-root.Mui-expanded": {
      minHeight: "40",
    },
    "& .MuiAccordionSummary-content.Mui-expanded": {
      marginTop: 10,
      marginBottom: 10,
    },
  },

  maxvalue: {
    borderWidth: 1,
    borderColor: "#7d7d7f",
    borderStyle: "solid",
    borderRadius: 5,
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "none",
    },
    "& .MuiInput-underline:after": {
      borderBottom: "none",
    },
    "& .MuiInput-underline:before": {
      borderBottom: "none",
    },

    maxtext: {
      color: "#797979",
      fontSize: "14",

      "& .MuiInputLabel-outlined": {
        lineHeight: 1,
      },
      "& .MuiOutlinedInput-input": {
        paddingLeft: 14,
        paddingRight: 14,
        paddingTop: 13,
        paddingBottom: 13,
      },
    },

    carimg: {
      height: "unset",
      width: "100",
      borderRadius: 0,
    },
    cartxt: {
      color: "#797979",
      fontSize: "14",
      textAlign: "center",
      marginTop: 5,
    },
    cardetail: {
      position: "relative",
    },
    closebtn: {
      position: "absolute",
      top: 0,
      right: 0,
    },

    btncard: {
      paddingTop: "0 !important",
    },
  },
});

const getRange = (entries) => {
  console.log("inside getLevels:", entries);

  let rangeValues = entries.map((val, i, arr) => {
    return parseInt(val.label);
  });

  console.log(
    "min max",
    Math.min(...rangeValues) + "||" + Math.max(...rangeValues)
  );
  return { min: Math.min(...rangeValues), max: Math.max(...rangeValues) };

  /* let sum = entries.reduce((sum, entry) => {
    console.log("acc, val:", sum + " | " + parseInt(entry.label));
    return parseInt(sum) + parseInt(entry.label);
  }, 0);
  console.log("inside sum:", sum);

  const counts = entries.reduce((sum, entry) => {
    console.log("sum, entry:", sum + " | " + entry.label);
    if (entry.count > 0) {
      console.log("entry count", entry);
      return [...sum, parseInt(entry.label)];
    }
    return sum;
  }, []);
  if (counts.length > 0) {
    return { min: Math.min(...counts), max: Math.max(...counts) };
  }
  return { min: 0, max: 0 }; */
};

const RangeFacet = ({ facet }) => {
  const api = useSearchkit();
  console.log("inside rangefacet:", facet.entries);
  const range = getRange(facet.entries);
  return (
    <div key={facet.identifier}>
      <Accordion style={{ marginBottom: 0, marginTop: 0 }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={useStyles.accorheading}>
            {facet.label}
          </Typography>
        </AccordionSummary>

        <AccordionDetails>
          <Typography>
            <Grid container spacing={2}>
              <Grid item sm={6}>
                <TextField
                  id="max"
                  label="Minimum"
                  type="text"
                  variant="outlined"
                  size="small"
                  className={useStyles.maxtext}
                  value="400"
                />
              </Grid>
              <Grid item sm={6}>
                <TextField
                  id="max"
                  label="Maximum"
                  type="text"
                  variant="outlined"
                  size="small"
                  className={useStyles.maxtext}
                  value="800"
                />
              </Grid>
              <Rangeslider min={range.min} max={range.max} />
            </Grid>
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

const RefinementFacet = ({ facet }) => {
  const api = useSearchkit();
  console.log("facets:", JSON.stringify(facet));

  return (
    <div key={facet.identifier}>
      <Accordion style={{ marginBottom: 0, marginTop: 0 }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={useStyles.accorheading}>
            {facet.label}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container spacing={2}>
            <Grid item sm={12}>
              <ThemeProvider theme={outerTheme}>
                <Checkbox
                  style={{ width: 16, height: 16 }}
                  defaultChecked
                  inputProps={{ "aria-label": "secondary checkbox" }}
                />
              </ThemeProvider>
              <Typography
                variant="caption"
                display="block"
                gutterBottom
                style={{ display: "inline-block", marginLeft: 10 }}
              >
                Select All
              </Typography>
            </Grid>
            {facet.entries.map((entry) => {
              const isSelected = api.isFilterSelected({
                identifier: facet.identifier,
                value: entry.label,
              });

              const toggleFilter = (filter) => {
                console.log("filtercheck: node mod: toggleFilter");
                if (api.isFilterSelected(filter)) {
                  //  this.removeFilter(filter);
                  console.log("filtercheck:  inside if ");
                  api.getFilters(filter);
                } else {
                  api.addFilter(filter);  
                }
              };

              const handleClick = (event) => {
                toggleFilter({
                  identifier: facet.identifier,
                  value: entry.label,
                });
                api.search();
              };

              return (
                <Grid
                  item
                  sm={4}
                  className={isSelected ? "cgSelected" : "cgNotSelected"}
                  key={entry.id}
                  onClick={handleClick}
                >
                  <Card className={useStyles.cardetail}>
                    <CardContent style={{ padding: 5 }}>
                      <Avatar
                        alt="Remy Sharp"
                        src="/static/images/avatar/c1.png"
                        style={{
                          margin: "0 auto",
                          width: "60px",
                          height: "unset",
                          borderRadius: "0",
                        }}
                      />
                      <Typography
                        variant="caption"
                        display="block"
                        gutterBottom
                        style={{ textAlign: "center", marginTop: 10 }}
                      >
                        {entry.label} - {entry.count}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              );
            })}
          </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

const CustomRefinementFacet = ({ facet }) => {
  const api = useSearchkit();
  console.log("facets:", JSON.stringify(facet));
};
const Facets = ({ data, loading }) => {
  console.log("print-data-Facets:... ", data);
  const [facetList, setFacetList] = useState([]);
  useEffect(() => {
    if (data) {
      console.log("print-inside-usereffect-facets:", data);
      setFacetList(data.facets);
    }
  }, [data]);
  return (
    <div>
      {facetList.map((facet) => {
        if (facet.__typename.localeCompare("RangeFacet") === 0) {
          //return <RangeFacet facet={facet} loading={loading} />;
        } else if (
          facet.__typename.localeCompare("RefinementSelectFacet") ===
          0 /* &&
          facet.identifier.localeCompare("bodyStyleName") === 0 */
        ) {
          return <RefinementFacet facet={facet} loading={loading} />;
        } else {
          //return <DynamicScopefilteritems facet={facet} loading={loading} />;
        }
      })}
    </div>
  );
};

export default Facets;

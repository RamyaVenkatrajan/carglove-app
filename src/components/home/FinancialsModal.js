import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import "../../App.css";
import Grid from "@material-ui/core/Grid";
//import { createMuiTheme } from "@material-ui/core/styles";
import { Scrollbars } from "react-custom-scrollbars";
import InputAdornment from "@material-ui/core/InputAdornment";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

/* const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#9c1233 !important",
      borderColor: "#9c1233",
      backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    },
  },
}); */

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 700,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 32px !important",
  },
  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
}));

const FinancialsModal = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const [disabled] = React.useState(true);
  const [financialValues, setFinancialValues] = React.useState([]);
  const [periodInYrsValue, setPeriodInYrsValue] = React.useState();
  const [periodInMnthsValue, setPeriodInMnthsValue] = React.useState();
  const [travellingpmValue, setTravellingpmValue] = React.useState();
  const [travellingpyValue, setTravellingpyValue] = React.useState();
  const [depositInPercentValue, setDepositInPercentValue] = React.useState();
  const [depositInRandValue, setDepositInRandValue] = React.useState();
  const [balloonInPercentValue, setBalloonInPercentValue] = React.useState();
  const [balloonInRandValue, setBalloonInRandValue] = React.useState();
  const [depositMaxValue, setDepositMaxValue] = React.useState();
  const [balloonMaxValue, setBalloonMaxValue] = React.useState();

  useEffect(() => {
    fetchData();
    console.log("loading--->"+loading);
  }, []);

  const fetchData = () => {
    setLoading(true);
    fetch(`http://localhost:3090/v1/data/financial`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response.data);
        console.log(
          "balloon in % --> " +
            response.data.financialResultSet[0].balloonInPercent
        );
        setFinancialValues(response.data.financialResultSet[0]);
        setLoading(false);
        setPeriodInYrsValue(response.data.financialResultSet[0].periodInYrs);
        setPeriodInMnthsValue(
          response.data.financialResultSet[0].periodInMnths
        );
        setTravellingpmValue(response.data.financialResultSet[0].travellingpm);
        setTravellingpyValue(response.data.financialResultSet[0].travellingpy);
        setDepositInPercentValue(
          response.data.financialResultSet[0].depositInPercent
        );
        setDepositInRandValue(
          response.data.financialResultSet[0].depositInRand
        );
        setBalloonInPercentValue(
          response.data.financialResultSet[0].balloonInPercent
        );
        setBalloonInRandValue(
          response.data.financialResultSet[0].balloonInRand
        );
        setDepositMaxValue("1000000");
        setBalloonMaxValue("1000000");
      })
      .catch((error) => console.log(error));
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    setOpen(false);
  };

  const handlePeriodInYrs = (event) => {
    setPeriodInYrsValue(event.target.value);
    handlePeriodInYrsToMnths(event.target.value);
  };

  const handlePeriodInYrsToMnths = (yearVal) => {
    setPeriodInMnthsValue("");
    const monthConversionValue = yearVal ? yearVal * 12 : periodInMnthsValue;
    setPeriodInMnthsValue(monthConversionValue);
  };

  const handlePeriodInMnths = (event) => {
    setPeriodInMnthsValue(event.target.value);
    handlePeriodInMnthsToYrs(event.target.value);
  };

  const handlePeriodInMnthsToYrs = (monVal) => {
    setPeriodInYrsValue("");
    const yearConversionValue = monVal
      ? Math.round(monVal / 12)
      : periodInYrsValue;
    setPeriodInYrsValue(yearConversionValue);
  };

  const handleTravellingpm = (event) => {
    setTravellingpmValue(event.target.value);
    handleTravellingpmTopy(event.target.value);
  };

  const handleTravellingpmTopy = (travelPm) => {
    setTravellingpyValue("");
    const travellingpmTopyConversionValue = travelPm
      ? travelPm * 12
      : travellingpyValue;
    setTravellingpyValue(travellingpmTopyConversionValue);
  };

  const handleTravellingpy = (event) => {
    setTravellingpyValue(event.target.value);
    handleTravellingpyTopm(event.target.value);
  };

  const handleTravellingpyTopm = (travelPy) => {
    setTravellingpmValue("");
    const travellingpyTopmConversionValue = travelPy
      ? Math.round(travelPy / 12)
      : travellingpmValue;
    setTravellingpmValue(travellingpyTopmConversionValue);
  };

  const handleDepositInPercent = (event) => {
    setDepositInPercentValue(event.target.value);
    handleDepositInPercentToRand(event.target.value);
  };

  const handleDepositInPercentToRand = (depositPercentVal) => {
    setDepositInRandValue("");
    const depositInPercentToRandConversionValue = Math.round(
      (depositMaxValue * depositPercentVal) / 100
    );
    setDepositInRandValue(depositInPercentToRandConversionValue);
  };

  const handleDepositInRand = (event) => {
    setDepositInRandValue(event.target.value);
    handleDepositInRandToPercent(event.target.value);
  };

  const handleDepositInRandToPercent = (depositRandVal) => {
    setDepositInPercentValue("");
    const depositInRandToPercentConversionValue = Math.round(
      (depositRandVal / depositMaxValue) * 100
    );
    setDepositInPercentValue(depositInRandToPercentConversionValue);
  };

  const handleBalloonInPercent = (event) => {
    setBalloonInPercentValue(event.target.value);
    handleBalloonInPercentToRand(event.target.value);
  };

  const handleBalloonInPercentToRand = (balloonPercentVal) => {
    setBalloonInRandValue("");
    const balloonInPercentToRandConversionValue = Math.round(
      (balloonMaxValue * balloonPercentVal) / 100
    );
    setBalloonInRandValue(balloonInPercentToRandConversionValue);
  };

  const handlBalloonInRand = (event) => {
    setBalloonInRandValue(event.target.value);
    handleBalloonInRandToPercent(event.target.value);
  };

  const handleBalloonInRandToPercent = (balloonRandVal) => {
    setBalloonInPercentValue("");
    const balloontInRandToPercentConversionValue = Math.round(
      (balloonRandVal / balloonMaxValue) * 100
    );
    setBalloonInPercentValue(balloontInRandToPercentConversionValue);
  };

  const body = (
    <div className="cardmodal">
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title">Financials</h2>
        <Chip label="x" className={classes.closebtn} onClick={handleClick} />
        <p id="simple-modal-description" class="finacialmodal">
          <Scrollbars style={{ height: 400 }}>
            <Grid container>
              <Grid container>
                <Grid item sm={12} style={{ marginTop: 10 }}>
                  <TextField
                    required
                    id="outlined-required"
                    label="Price"
                    variant="outlined"
                    className="modalfields"
                    defaultValue={depositMaxValue}
                    disabled={disabled}
                  />
                </Grid>
              </Grid>

              <Grid item sm={6} style={{ marginTop: 10 }}>
                <TextField
                  required
                  id="outlined-required"
                  label="Interest Rate"
                  variant="outlined"
                  className="modalfields"
                  defaultValue={financialValues.interestRate}
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item sm={6}>
                <TextField
                  required="true"
                  id="outlined-required"
                  label="Period(in yr)"
                  variant="outlined"
                  className="modalfields"
                  value={periodInYrsValue}
                  onChange={handlePeriodInYrs}
                />
              </Grid>
              <Grid item sm={6}>
                <TextField
                  required="true"
                  id="outlined-required"
                  label="Period(in months)"
                  variant="outlined"
                  className="modalfields"
                  value={periodInMnthsValue}
                  onChange={handlePeriodInMnths}
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Deposit(in %)"
                  variant="outlined"
                  className="modalfields"
                  value={depositInPercentValue}
                  onChange={handleDepositInPercent}
                />
              </Grid>
              <Grid item sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Deposit(in rand)"
                  variant="outlined"
                  className="modalfields notation"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  value={depositInRandValue}
                  onChange={handleDepositInRand}
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Balloon(in %)"
                  variant="outlined"
                  className="modalfields"
                  value={balloonInPercentValue}
                  onChange={handleBalloonInPercent}
                />
              </Grid>
              <Grid item sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Balloon(in rand)"
                  variant="outlined"
                  className="modalfields notation"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">R</InputAdornment>
                    ),
                  }}
                  value={balloonInRandValue}
                  onChange={handlBalloonInRand}
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item sm={6}>
                <TextField
                  required="true"
                  id="outlined-required"
                  label="Travelling(p/y)"
                  variant="outlined"
                  className="modalfields"
                  value={travellingpyValue}
                  onChange={handleTravellingpy}
                />
              </Grid>
              <Grid item sm={6}>
                <TextField
                  required="true"
                  id="outlined-required"
                  label="Travelling(p/m)"
                  variant="outlined"
                  className="modalfields"
                  value={travellingpmValue}
                  onChange={handleTravellingpm}
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Insurance(in rand)"
                  variant="outlined"
                  className="modalfields"
                  defaultValue={financialValues.insurance}
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Petrol 95"
                  variant="outlined"
                  className="modalfields"
                  disabled={disabled}
                  defaultValue={financialValues.petrol95}
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Diesel 50ppm"
                  variant="outlined"
                  className="modalfields"
                  disabled={disabled}
                  defaultValue={financialValues.diesel50ppm}
                />
              </Grid>
            </Grid>
          </Scrollbars>

          <Grid container>
            <Grid item xs style={{ marginRight: 10 }}>
              <Button variant="contained" className={classes.textbtns}>
                Submit
              </Button>
            </Grid>
            <Grid item xs>
              <Button variant="contained" className={classes.textbtns}>
                Cancel
              </Button>
            </Grid>
          </Grid>
        </p>
      </div>
    </div>
  );

  return (
    <div>
      <Button
        variant="contained"
        onClick={handleOpen}
        color="primary"
        size="small"
        style={{
          marginRight: 10,
          textTransform: "capitalize",
          fontSize: 14,
          marginTop: 20,
        }}
      >
        Financials
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
};

export default FinancialsModal;

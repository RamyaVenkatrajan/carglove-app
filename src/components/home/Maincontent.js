import React from "react";
import {
  withStyles,
  createMuiTheme,
  ThemeProvider,
} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Resulttable2 from "./Resulttable2";
import Resulttable1 from "./Resulttable1";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Preference from "./Preference";
import FinancialsModal from "./FinancialsModal";
import CardetailModal from "../carDetail/CardetailModal";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#9c1233 !important",
      borderColor: "#9c1233",
      backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    },
  },
});
const useStyles = (theme) => ({
  spacing: {
    paddingRight: 10,
    paddingLeft: 10,
  },
  m20: {
    marginTop: 20,
  },
});

class Maincontent extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Preference />
        <Typography
          variant="caption"
          display="block"
          gutterBottom
          style={{
            display: "inline-block",
            fontSize: 16,
            color: "#000",
            marginBottom: 20,
            marginTop: 20,
          }}
        >
          Search Results
        </Typography>
        <ThemeProvider theme={theme}>
          <ButtonGroup style={{ float: "right" }}>
            <Button
              variant="contained"
              color="primary"
              size="small"
              style={{
                marginRight: 10,
                textTransform: "capitalize",
                fontSize: 14,
                marginTop: 20,
              }}
            >
              Complete
            </Button>
            <FinancialsModal />
          </ButtonGroup>
        </ThemeProvider>

        <Resulttable1 />
      </div>
    );
  }
}

export default withStyles(useStyles)(Maincontent);

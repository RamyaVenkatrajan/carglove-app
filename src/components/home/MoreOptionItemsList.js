import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import MuseumIcon from "@material-ui/icons/Museum";
import AddCircleIcon from '@material-ui/icons/AddCircle';
const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
  leftIcon: {
    fontSize: 14,
  },
  rightIcon: {
    fontSize: 14,
  },

  poppverbtns: {
    textTransform: "capitalize",
    borderColor: "#9c1233",
    width: "100%",
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  iconpoppverbtns: {
    textTransform: "capitalize",
    borderColor: "#9c1233",
    width: 45,
    color: "#00000042",
  },

  textbtns: {
    textTransform: "capitalize",
    borderColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  onchangetextbtns: {
    textTransform: "capitalize",
    backgroundColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },

  closebtn: {
    position: "absolute",
    right: -6,
    top: -6,
    color: "#4caf50",
    fontSize: 18
  },
}));

export default function MoreOptionItemsList() {
  const classes = useStyles();
   const [anchorEl, setAnchorEl] = React.useState(null);
  const [btnActive, setBtnActive] = React.useState("buttonActive");
  const [loading, setLoading] = useState(false);
  const [scopeValues, setScopeValues] = useState([]);
  const handleClick = (event) => {
   console.log("Inside handle click");
  };

  const changeBtnColor = () => {
    setBtnActive("changeBtnColor");
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
   useEffect(() => {
    fetchData();
  }, []);
  const fetchData = () => {
    setLoading(true);
    fetch(`http://localhost:3090/v1/data/scope`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response.data);
        console.log("scope res --> " + response.data.scopeResultSet[0].name);
        setScopeValues(response.data.scopeResultSet);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  return (
    <div>
      <Grid container direction="column" alignItems="center">
        <Grid item sm={12} style={{ width: "100%" }}>
        {Object.keys(scopeValues)
              .map((key) => ({ id: key, ...scopeValues[key] }))
              .filter((scope) => scope.visibility === "optional")
              .map((scope) => (
          <ButtonGroup
            className={classes.poppverbtns}
            variant="contained"
            aria-label="split button"
             style={{ marginTop: 20 }}
          >
            <Button className={classes.iconpoppverbtns} size="small">
              <MuseumIcon />
            </Button>
            <Button
            disabled
              aria-describedby={id}
              variant="contained"
              onClick={handleClick}
              className={
                btnActive === "changeBtnColor"
                  ? classes.onchangetextbtns
                  : classes.textbtns
              }
              key={scope.seqNo}
            >
              {scope.name}
              <AddCircleIcon className={classes.closebtn}/>
            </Button>
          </ButtonGroup>
           ))}
        </Grid>
      </Grid>
    </div>
  );
}

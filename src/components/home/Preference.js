import React, { useState, useEffect } from "react";
import DragItem from "../drag-item";
import DropItem from "../drop-item";
import "../../App.css";
import Grid from "@material-ui/core/Grid";
import { Scrollbars } from "react-custom-scrollbars";



const App = () => {
  const [carValues, setCarValue] = useState([]);
  const [today] = useState(new Date());
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = () => {
    setLoading(true);
    fetch(`http://localhost:3090/v1/data/preference`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response.data);
        console.log("ressssss --> " + response.data);
        setCarValue(response.data.resultSet);
        setLoading(false);
        console.log("ressssss --> " + carValues);
      })
      .catch((error) => console.log(error));
  };
  const scrollContainerStyle = {
    width: "auto",
    height: "295px",
    overflow: "auto",
    whiteSpace: "nowrap",
    background: "#dfe3e6",
  };

  /*   useEffect(() => {
    fetch(`http://localhost:3090/v1/data/preference`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response.data);
        setValue(response.data);
        //setIsLoading(false);
      })
      .catch((error) => console.log(error));
  }); */
  return (
    <div className="App">
      <div className="box">
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={9} lg={10}>
            <div className="Preferenceside1">
              <p style={{ fontSize: 16, textAlign: "center" }}>
                {" "}
                My Analysis {today.toLocaleString()}{" "}
              </p>

              <div
                className="scrollbar scrollbar-primary mx-auto"
                style={scrollContainerStyle}
              >
                <DropItem
                  heading="Preference"
                  onDrop={(id) => {
                    const currentCar = { ...carValues[id-1] };
                    currentCar.visibility = "default";
                    setCarValue({ ...carValues, ...{ [id-1]: currentCar } });
                  }}
                >
                  {Object.keys(carValues)
                    .map((key) => ({ id: key, ...carValues[key] }))
                    .filter((car) => car.visibility === "default")
                    .map((car) => (
                      <DragItem id={car.seqNo} data={car} key={car.seqNo} />
                    ))}
                </DropItem>
              </div>
            </div>
          </Grid>

          <Grid item xs={12} sm={12} md={3} lg={2}>
            <div className="Preferenceside2">
              <p> PreferenceList </p>
              <Scrollbars style={{ height: 295 }}>
                <DropItem
                  heading="PreferenceList"
                  onDrop={(id) => {
                    const currentCar = { ...carValues[id-1] };
                    currentCar.visibility = "optional";
                    setCarValue({ ...carValues, ...{ [id-1]: currentCar } });
                  }}
                >
                  {Object.keys(carValues)
                    .map((key) => ({ id: key, ...carValues[key] }))
                    .filter((car) => car.visibility === "optional" )
                    .map((car) => (
                      <DragItem id={car.seqNo} data={car} key={car.seqNo} />
                    ))}
                </DropItem>
              </Scrollbars>
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}

export default App;

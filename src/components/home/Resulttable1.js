import React from "react";
import { gql } from "@apollo/client";
import { useSearchkitQuery, useSearchkit } from "@searchkit/client";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Avatar from "@material-ui/core/Avatar";
import CardetailModal from "../carDetail/CardetailModal";

const QUERY = gql`
  query resultSet(
    $query: String
    $filters: [FiltersSet]
    $page: PageInput
    $sortBy: String
  ) {
    results(query: $query, filters: $filters) {
      summary {
        total
        appliedFilters {
          id
          identifier
          display
          label
          ... on DateRangeSelectedFilter {
            dateMin
            dateMax
          }
          ... on NumericRangeSelectedFilter {
            min
            max
          }
          ... on ValueSelectedFilter {
            value
          }
        }
        sortOptions {
          id
          label
        }
        query
      }
      hits(page: $page, sortBy: $sortBy) {
        page {
          total
          totalPages
          pageNumber
          from
          size
        }
        sortedBy
        items {
          id
          fields {
            modelId
            model_data {
              modelName
              price
              monthlyExp
              monthlyInstallment
              seats
              doors
              country {
                name
              }
              bodyStyle {
                name
              }
            }
          }
        }
      }
      facets {
        identifier
        type
        label
        display
        entries {
          id
          label
          count
        }
      }
    }
  }
`;

const useRowStyles = makeStyles({
  root: {
    "& > *": {
      borderBottom: "unset",
    },
  },
});

function Row(props) {
  const { item } = props;
  console.log("Item value-->"+item);
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow hover="true" className="resulttablerow" >
                  <TableCell align="left">
                    {" "}
                    <Avatar
                      alt="Remy Sharp"
                      src="/static/images/avatar/c1.png"
                      style={{
                        margin: "0 auto",
                        width: "60px",
                        height: "unset",
                        borderRadius: "0",
                      }}
                    />
                  </TableCell>
                  <TableCell align="left" >100%</TableCell>
                  <TableCell align="left">92.12</TableCell>
                  <TableCell align="left">
                    {item.fields.model_data.modelName}
                  </TableCell>
                  <TableCell align="left">
                    {item.fields.model_data.price}
                  </TableCell>
                  <TableCell align="left">
                    {item.fields.model_data.monthlyInstallment}
                  </TableCell>
                  <TableCell align="left">
                    {item.fields.model_data.monthlyExp}
                  </TableCell>
                  <TableCell align="left">
                    {item.fields.model_data.bodyStyle.name}
                  </TableCell>
                  <TableCell align="left">
                    {item.fields.model_data.doors}
                  </TableCell>
                  <TableCell align="left">
                    <CardetailModal data={item.fields.modelId} />
                  </TableCell>
                  <TableCell align="left">
                    <IconButton
                      aria-label="expand row"
                      size="small"
                      onClick={() => setOpen(!open)}
                    >
                      {open ? (
                        <KeyboardArrowUpIcon />
                      ) : (
                        <KeyboardArrowDownIcon />
                      )}
                    </IconButton>
                  </TableCell>
              </TableRow>  

              <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Table size="small" aria-label="purchases" className="collapsibletable">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">Model</TableCell>
                    <TableCell align="left">Door</TableCell>
                    <TableCell align="left">Body style</TableCell>
                  </TableRow>

                <TableBody>
                    <TableRow>
                      <TableCell align="left">  {item.fields.model_data.modelName} </TableCell>
                      <TableCell align="left">  {item.fields.model_data.doors}</TableCell>
                      <TableCell align="left">  {item.fields.model_data.bodyStyle.name}  </TableCell>
                    </TableRow>
                </TableBody> 

                </TableHead>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}



export default function Resulttable1() {
  const { data, loading } = useSearchkitQuery(QUERY);
  const [open, setOpen] = React.useState(false);

  if (loading || !data) {
    return <h1>loading...</h1>;
  }
  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow className="resultheaderrow">
            <TableCell>Image</TableCell>
            <TableCell align="left">Confidence</TableCell>
            <TableCell align="left">Rank</TableCell>
            <TableCell align="left">Model</TableCell>
            <TableCell align="left">Price(R)</TableCell>
            <TableCell align="left">Installment</TableCell>
            <TableCell align="left">Monthly Expenditure</TableCell>
            <TableCell align="left">Body Style</TableCell>
            <TableCell align="left">Doors</TableCell>
            <TableCell align="left">More Details</TableCell>
            <TableCell align="left">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.results &&
            data.results.hits.items.map((item) => (
              <Row key={item.fields.modelId} item={item} />
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

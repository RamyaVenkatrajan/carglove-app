import React from 'react';
import { gql } from '@apollo/client';
import { useSearchkitQuery, useSearchkit } from '@searchkit/client'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Avatar from "@material-ui/core/Avatar";
import CardetailModal from "../carDetail/CardetailModal";

const QUERY = gql`
query resultSet(
    $query: String
    $filters: [FiltersSet]
    $page: PageInput
    $sortBy: String
  ) {
    results(query: $query, filters: $filters) {
      summary {
        total
        appliedFilters {
          id
          identifier
          display
          label
          ... on DateRangeSelectedFilter {
            dateMin
            dateMax
          }
          ... on NumericRangeSelectedFilter {
            min
            max
          }
          ... on ValueSelectedFilter {
            value
          }
        }
        sortOptions {
          id
          label
        }
        query
      }
      hits(page: $page, sortBy: $sortBy) {
        page {
          total
          totalPages
          pageNumber
          from
          size
        }
        sortedBy
        items {
          id
          fields {
            modelId
            model_data{
              modelName
              price
              monthlyExp
              monthlyInstallment
              seats
              doors
              country{
                name
              }
              bodyStyle{
                name
              }
            }
          }
        }
      }
      facets {
        identifier
        type
        label
        display
        entries {
          id
          label
          count
        }
      }
    }
  }
  `;
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});


function createData(confidence, rank, model, price, installment, monthlyexp, bodystyle, doors) {
  return { confidence, rank, model, price, installment, monthlyexp, bodystyle, doors };
}

const rows = [
  createData('2594', 64.5, 'Audi', 'R75 500', 'R7542', 'R12 400', 'Hatch', 5),
  createData('2595', 64.5, 'Audi', 'R75 500', 'R7542', 'R12 400', 'Hatch', 5),
  createData('2596', 64.5, 'Audi', 'R75 500', 'R7542', 'R12 400', 'Hatch', 5),
  createData('2597', 64.5, 'Audi', 'R75 500', 'R7542', 'R12 400', 'Hatch', 5),
  createData('2598', 64.5, 'Audi', 'R75 500', 'R7542', 'R12 400', 'Hatch', 5),
 
];

export default function Resulttable1() {
  const classes = useStyles();
  const { data, loading } = useSearchkitQuery(QUERY)

  if (loading || !data) {
    return <h1>loading...</h1>;
  }
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Image</TableCell>
            <TableCell align="left">Confidence</TableCell>
            <TableCell align="left">Rank</TableCell>
            <TableCell align="left">Model</TableCell>
            <TableCell align="left">Price</TableCell>
            <TableCell align="left">Installment</TableCell>
            <TableCell align="left">Monthly Expenditure</TableCell>
            <TableCell align="left">Body Style</TableCell>
            <TableCell align="left">Doors</TableCell>
            <TableCell>More Details</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {
           data.results && data.results.hits.items.map((item) =>{
               return(
            <TableRow  hover="true" >
              <TableCell align="left" >  <Avatar
                      alt="Remy Sharp"
                      src="/static/images/avatar/c1.png"
                      style={{
                        margin: "0 auto",
                        width: "60px",
                        height: "unset",
                        borderRadius: "0",
                      }}/>
                    
                      
                      </TableCell>
              <TableCell align="left">100%</TableCell>
              <TableCell align="left">92.12</TableCell>
              <TableCell align="left">{item.fields.model_data.modelName}</TableCell>
              <TableCell align="left">{item.fields.model_data.price}</TableCell>
              <TableCell align="left">{item.fields.model_data.monthlyInstallment}</TableCell>
              <TableCell align="left">{item.fields.model_data.monthlyExp}</TableCell>
              <TableCell align="left">{item.fields.model_data.bodyStyle.name}</TableCell>
              <TableCell align="left">{item.fields.model_data.doors}</TableCell>
              <TableCell align="left"><CardetailModal data={item.fields.modelId}/></TableCell>
              
            </TableRow>
           )
        })
      }
        </TableBody>
      </Table>
    </TableContainer>
  )
}



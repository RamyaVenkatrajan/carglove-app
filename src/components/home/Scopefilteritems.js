import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import MoreOptionItemList from "./MoreOptionItemsList";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Chip from "@material-ui/core/Chip";
import AirlineSeatReclineExtraIcon from "@material-ui/icons/AirlineSeatReclineExtra";
import MuseumIcon from "@material-ui/icons/Museum";
import LanguageIcon from "@material-ui/icons/Language";
import TransformIcon from "@material-ui/icons/Transform";
import AirportShuttleIcon from "@material-ui/icons/AirportShuttle";
const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
  leftIcon: {
    fontSize: 14,
  },
  rightIcon: {
    fontSize: 14,
  },

  poppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  iconpoppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: 45,
    color: "#fff",
  },

  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  onchangetextbtns: {
    textTransform: "capitalize",
    // backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    // borderColor: "#9c1233",
    backgroundColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },

  moretoptionbtn: {
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    color: "#fff",
    marginTop: 20,
    height: 60,
    borderRadius: 4,
    maxWidth: "100%",
    width: "100%",
  },

  closebtn: {
    width: 15,
    height: 15,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
  },
}));

export default function Scopefilteritems() {
  const classes = useStyles();
  const [flag, setFlag] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [btnActive, setBtnActive] = React.useState("buttonActive");
  const [showResults, setShowResults] = React.useState(false);
  const onClick = () => setShowResults(!showResults);

  const handleClick = (event) => {
    //   setBtnActive('buttonActive');
    setAnchorEl(event.currentTarget);
    // setIsActive(!isActive);
    //   handleOnClick();
    //   handleOffClick();
  };

  //   const handleOnClick = () => { console.log("Inside on click");}
  //  const handleOffClick = () => { console.log("Inside off click");}

  const handleClose = () => {
    console.log("close items");
    setAnchorEl(null);
  };

  const changeBtnColor = () => {
    setBtnActive("changeBtnColor");
  };

  const changeBackBtnColor = () => {
    setBtnActive("changeBackBtnColor");
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <Grid container direction="column" alignItems="center">
        <Grid item sm={12} style={{ width: "100%" }}>
          <ButtonGroup
            className={classes.poppverbtns}
            variant="contained"
            aria-label="split button"
          >
            <Button className={classes.iconpoppverbtns} size="small">
              <MuseumIcon />
            </Button>
            <Button
              aria-describedby={id}
              variant="contained"
              onClick={handleClick}
              className={
                btnActive === "changeBtnColor"
                  ? classes.onchangetextbtns
                  : classes.textbtns
              }
              // className={classes.textbtns}
              //  className={isActive ? "popoverisactive" : classes.textbtns}
            >
              Manufacturer
              <Chip label="x" className={classes.closebtn} />
            </Button>
          </ButtonGroup>

          <ButtonGroup
            variant="contained"
            className={classes.poppverbtns}
            aria-label="split button"
            style={{ marginTop: 20 }}
          >
            <Button className={classes.iconpoppverbtns} size="small">
              <AirportShuttleIcon />
            </Button>
            <Button
              aria-describedby={id}
              variant="contained"
              className={classes.textbtns}
              onClick={handleClick}
            >
              Doors
              <Chip label="x" className={classes.closebtn} />
            </Button>
          </ButtonGroup>

          <ButtonGroup
            variant="contained"
            className={classes.poppverbtns}
            aria-label="split button"
            style={{ marginTop: 20 }}
          >
            <Button className={classes.iconpoppverbtns} size="small">
              <AirlineSeatReclineExtraIcon />
            </Button>
            <Button
              aria-describedby={id}
              variant="contained"
              className={classes.textbtns}
              onClick={handleClick}
            >
              Seats
              <Chip label="x" className={classes.closebtn} />
            </Button>
          </ButtonGroup>

          <ButtonGroup
            variant="contained"
            className={classes.poppverbtns}
            aria-label="split button"
            style={{ marginTop: 20 }}
          >
            <Button className={classes.iconpoppverbtns} size="small">
              <TransformIcon />
            </Button>
            <Button
              aria-describedby={id}
              variant="contained"
              className={classes.textbtns}
              onClick={handleClick}
            >
              Transmission
              <Chip label="x" className={classes.closebtn} />
            </Button>
          </ButtonGroup>

          <ButtonGroup
            variant="contained"
            className={classes.poppverbtns}
            aria-label="split button"
            style={{ marginTop: 20 }}
          >
            <Button className={classes.iconpoppverbtns} size="small">
              <LanguageIcon />
            </Button>
            <Button
              aria-describedby={id}
              variant="contained"
              className={classes.textbtns}
              onClick={handleClick}
            >
              Country
              <Chip label="x" className={classes.closebtn} />
            </Button>
          </ButtonGroup>

          <Button
            aria-describedby={id}
            variant="contained"
            className={classes.moretoptionbtn}
            onClick={onClick}
            style={{ borderRight: 0 }}
          >
           {showResults ? "Hide Options" : "More Options"}
          </Button>
          {showResults ? <MoreOptionItemList /> : null}
        </Grid>
      </Grid>
    </div>
  );
}

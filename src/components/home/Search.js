import React from "react";
import {
  withStyles,
  createMuiTheme,
  ThemeProvider,
} from "@material-ui/core/styles";
//styles-import
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import Preference from "./Preference";
import FinancialsModal from "./FinancialsModal";
import { makeStyles } from '@material-ui/styles';
import CardetailModal from "../carDetail/CardetailModal";
import Avatar from "@material-ui/core/Avatar";
import Collapse from "@material-ui/core/Collapse";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Box from "@material-ui/core/Box";
import Sidebar from './Sidebar'
import {
  ReactiveBase,
  ReactiveList,
} from '@appbaseio/reactivesearch';

//theme
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#9c1233 !important",
      borderColor: "#9c1233",
      backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    },
  },
});

//styles
const useStyles = () => ({
accorheading: {
  fontSize: 16,
  color: "#4f4f4f",
},
sidebaraccordion: {
  marginTop: "0 !important",
  marginBottom: "0 !important",
  "& .MuiAccordionSummary-root.Mui-expanded": {
    minHeight: "40",
  },
  "& .MuiAccordionSummary-content.Mui-expanded": {
    marginTop: 10,
    marginBottom: 10,
  },
},

maxvalue: {
  borderWidth: 1,
  borderColor: "#7d7d7f",
  borderStyle: "solid",
  borderRadius: 5,
  "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
    borderBottom: "none",
  },
  "& .MuiInput-underline:after": {
    borderBottom: "none",
  },
  "& .MuiInput-underline:before": {
    borderBottom: "none",
  },

  maxtext: {
    color: "#797979",
    fontSize: "14",

    "& .MuiInputLabel-outlined": {
      lineHeight: 1,
    },
    "& .MuiOutlinedInput-input": {
      paddingLeft: 14,
      paddingRight: 14,
      paddingTop: 13,
      paddingBottom: 13,
    },
  },

  carimg: {
    height: "unset",
    width: "100",
    borderRadius: 0,
  },
  cartxt: {
    color: "#797979",
    fontSize: "14",
    textAlign: "center",
    marginTop: 5,
  },
  cardetail: {
    position: "relative",
  },
  closebtn: {
    position: "absolute",
    top: 0,
    right: 0,
  },

  btncard: {
    paddingTop: "0 !important",
  },
},
});
const useStylesContainer = makeStyles({
    spacing: {
      paddingRight: 10,
      paddingLeft: 10,
      marginTop: 20,
    },
    m20: {},
});


function Row(props) {
  const { item } = props;
  console.log("Item value-->"+item);
  const [open, setOpen] = React.useState(false);

  return (
    <React.Fragment>
      <TableRow hover="true" className="resulttablerow" >
                  <TableCell align="left">
                   
                    <Avatar
                      alt="Remy Sharp"
                      src="/static/images/avatar/c1.png"
                      style={{
                        margin: "0 auto",
                        width: "60px",
                        height: "unset",
                        borderRadius: "0",
                      }}
                    />
                  </TableCell>
                  <TableCell align="left" >100%</TableCell>
                  <TableCell align="left">92.12</TableCell>
                  <TableCell align="left">
                    {item.model_data.modelName}
                  </TableCell>
                  <TableCell align="left">
                    {item.model_data.price}
                  </TableCell>
                  <TableCell align="left">
                    {item.model_data.monthlyInstallment}
                  </TableCell>
                  <TableCell align="left">
                    {item.model_data.monthlyExp}
                  </TableCell>
                  <TableCell align="left">
                    {item.model_data.bodyStyle.name}
                  </TableCell>
                  <TableCell align="left">
                    {item.model_data.doors}
                  </TableCell>
                  <TableCell align="left">
                    <CardetailModal data={item.modelId} />
                  </TableCell>
                  <TableCell align="left">
                    <IconButton
                      aria-label="expand row"
                      size="small"
                      onClick={() => setOpen(!open)}
                    >
                      {open ? (
                        <KeyboardArrowUpIcon />
                      ) : (
                        <KeyboardArrowDownIcon />
                      )}
                    </IconButton>
                  </TableCell>
              </TableRow>  

              <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Table size="small" aria-label="purchases" className="collapsibletable">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">Model</TableCell>
                    <TableCell align="left">Door</TableCell>
                    <TableCell align="left">Body style</TableCell>
                  </TableRow>

                <TableBody>
                    <TableRow>
                      <TableCell align="left">  {item.model_data.modelName} </TableCell>
                      <TableCell align="left">  {item.model_data.doors}</TableCell>
                      <TableCell align="left">  {item.model_data.bodyStyle.name}  </TableCell>
                    </TableRow>
                </TableBody> 

                </TableHead>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

const Search = (props) => {
const classes = useStylesContainer(props);
return (
  <div className="content">
    <ReactiveBase
      app="fb_carglove_sk"
      url="http://localhost:9200"
      // enableAppbase
  >
    <Grid container className="filtercontainer">
    <Grid item xs={12} sm={12} md={3} lg={3} className={classes.spacing}>
    <Sidebar />
    </Grid>
    <Grid item xs={12} sm={12} md={9} lg={9} className={classes.spacing} >
        <div>
        <Preference />
      <Typography
        variant="caption"
        display="block"
        gutterBottom
        style={{
          display: "inline-block",
          fontSize: 16,
          color: "#000",
          marginBottom: 20,
          marginTop: 20,
        }}
      >
        Search Results
      </Typography>
      <ThemeProvider theme={theme}>
        <ButtonGroup style={{ float: "right" }}>
          <Button
            variant="contained"
            color="primary"
            size="small"
            style={{
              marginRight: 10,
              textTransform: "capitalize",
              fontSize: 14,
              marginTop: 20,
            }}
          >
            Complete
          </Button>
          <FinancialsModal />
        </ButtonGroup>
      </ThemeProvider>          
    <TableContainer component={Paper}>    
    <Table aria-label="collapsible table">
      <TableHead>
        <TableRow className="resultheaderrow">
          <TableCell>Image</TableCell>
          <TableCell align="left">Confidence</TableCell>
          <TableCell align="left">Rank</TableCell>
          <TableCell align="left">Model</TableCell>
          <TableCell align="left">Price(R)</TableCell>
          <TableCell align="left">Installment</TableCell>
          <TableCell align="left">Monthly Expenditure</TableCell>
          <TableCell align="left">Body Style</TableCell>
          <TableCell align="left">Doors</TableCell>
          <TableCell align="left">More Details</TableCell>
          <TableCell align="left">Action</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
      <ReactiveList
      componentId="result"
      title="Results"
      dataField="model_data.model"
      from={0}
      size={10}
      showResultStats={false}
      defaultQuery={() => {
        return  {
          "script_fields": {
            "monthlyInstallment": {
              "script": {
                "lang":   "expression",
                "source":"(((doc['model_data.price'] -((doc['model_data.price']*d)/100))*(pow((1+(r/1200)),n)) - ((doc['model_data.price']*b)/100))*(r/1200))/(pow((1+(r/1200)),n)-1)",
                 "params": {
                  "b": 50,"d":15,"r":7.25 ,"n":48
                }
              }
            }
            }
        };
    }}
      react={{
        and : ['result','PriceSensor','SeatsSensor',"DoorsSensor","TransmissionsSensor","CountrySensor","ManufacturerSensor"]
      }}
      render={({ data }) => (
        <ReactiveList.ResultListWrapper>
              {data.map(item => (
                  <Row key={item.modelId} item={item} />
              ))}
         	</ReactiveList.ResultListWrapper>
      )}     
  />
      </TableBody>
    </Table>
  </TableContainer>
  </div>
    </Grid>

  </Grid>
  </ReactiveBase>
  </div>
);
};

export default withStyles(useStyles)(Search);

//react-import
import React from "react";
//styles-import
import { makeStyles } from "@material-ui/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import Popover from "@material-ui/core/Popover";
import Chip from "@material-ui/core/Chip";
import AirlineSeatReclineExtraIcon from "@material-ui/icons/AirlineSeatReclineExtra";
import MuseumIcon from "@material-ui/icons/Museum";
import LanguageIcon from "@material-ui/icons/Language";
import TransformIcon from "@material-ui/icons/Transform";
import AirportShuttleIcon from "@material-ui/icons/AirportShuttle";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemIcon from "@material-ui/core/ListItemIcon";


import {
  RangeInput,
  MultiDropdownList,
  SelectedFilters,
  MultiList,
} from "@appbaseio/reactivesearch";
//theme
//styles
const useStyles = makeStyles(() => ({
  poppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
    marginTop: 20,
  },

  iconpoppverbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: 45,
    color: "#fff",
  },

  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },

  onchangetextbtns: {
    textTransform: "capitalize",
    // backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    // borderColor: "#9c1233",
    backgroundColor: "#9c1233",
    maxWidth: "100%",
    width: "100%",
    justifyContent: "flex-start",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
      backgroundColor: "#9c1233",
    },
  },

  moretoptionbtn: {
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    color: "#fff",
    marginTop: 20,
    height: 60,
    borderRadius: 4,
    maxWidth: "100%",
    width: "100%",
  },

  closebtn: {
    width: 15,
    height: 15,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
  },
}));

const Sidebar = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  const [btnActive, setBtnActive] = React.useState("buttonActive");
  const [dataFieldValue, setDataFieldValue] = React.useState("");
  const [checked, setChecked] = React.useState(true);
   const handleChange = (event) => {
    setChecked(event.target.checked);
  };
  const handleManufacturerClick = (event) => {
    setAnchorEl(event.currentTarget);
    setDataFieldValue("model_data.manufacturer.name.keyword");
  };

  const handleDoorsClick = (event) => {
    setAnchorEl(event.currentTarget);
     setDataFieldValue("model_data.doors");
  };

  const handleCountryClick = (event) => {
    setAnchorEl(event.currentTarget);
    setDataFieldValue("model_data.country.name.keyword");
  };

  const handleSeatsClick = (event) => {
    setAnchorEl(event.currentTarget);
    setDataFieldValue("model_data.seats");
  };

   const handleTransmissionClick = (event) => {
    setAnchorEl(event.currentTarget);
    setDataFieldValue("model_data.transmission.keyword");
  };


  const handleClose = () => {
    console.log("close items");
    setAnchorEl(null);
  };

  const changeBtnColor = () => {
    setBtnActive("changeBtnColor");
  };

  const changeBackBtnColor = () => {
    setBtnActive("changeBackBtnColor");
  };

  return (
    <div>
      <Card className="filter_sidebar sidebar_section">
        <CardContent className="sidebar_header">
          <CardActions>
            <p>Filters</p>
          </CardActions>
        </CardContent>
      </Card>

      <Card style={{ borderRadius: 0 }}>
        <SelectedFilters
          render={({ clearValues }) => (
            <ButtonGroup style={{ float: "right" }}>
              <Button
                variant="contained"
                color="primary"
                size="small"
                style={{
                  marginRight: 10,
                  textTransform: "capitalize",
                  fontSize: 14,
                  marginTop: 10,
                }}
                onClick={clearValues}
              >
                Reset
              </Button>
            </ButtonGroup>
          )}
        />
        <CardContent style={{ display: "inline-block" }}>
          <Accordion defaultExpanded className={useStyles.sidebaraccordion}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={useStyles.accorheading}>Price</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <Grid container spacing={2}>
                  <RangeInput
                    dataField="model_data.price"
                    componentId="PriceSensor"
                    tooltipTrigger="hover"
                    range={{
                      start: 100000,
                      end: 5000000,
                    }}
                    defaultValue={{
                      start: 100000,
                      end: 5000000,
                    }}
                    rangeLabels={{
                      start: "1L",
                      end: "50L",
                    }}
                    showHistogram={false}
                    react={{
                      and: [
                        "result",
                        "SeatsSensor",
                        "DoorsSensor",
                        "TransmissionsSensor",
                        "ManufacturerSensor",
                      ],
                    }}
                    className="custom-range-slider"
                    innerClass={{
                      slider: "custom-range-slider-bar",
                    }}
                  />
                </Grid>
              </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion style={{ marginBottom: 0, marginTop: 0 }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography className={useStyles.accorheading}>
                Monthly Installments
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <Grid container spacing={2}>
                  <RangeInput
                    dataField="model_data.monthlyInstallment"
                    componentId="InstallmentSensor"
                    tooltipTrigger="hover"
                    range={{
                      start: 50000,
                      end: 500000,
                    }}
                    defaultValue={{
                      start: 50000,
                      end: 500000,
                    }}
                    rangeLabels={{
                      start: "50K",
                      end: "5L",
                    }}
                    showHistogram={false}
                    react={{
                      and: [
                        "result",
                        "PriceSensor",
                        "InstallmentSensor",
                        "monthlyExpSensor",
                        "DoorsSensor",
                        "TransmissionsSensor",
                      ],
                    }}
                    className="custom-range-slider"
                    innerClass={{
                      slider: "custom-range-slider-bar",
                    }}
                  />
                </Grid>
              </Typography>
            </AccordionDetails>
          </Accordion>

          <Accordion style={{ marginBottom: 0, marginTop: 0 }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography className={useStyles.accorheading}>
                Monthly Expenditure
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <Grid container spacing={2}>
                  <RangeInput
                    dataField="model_data.monthlyExp"
                    componentId="monthlyExpSensor"
                    tooltipTrigger="hover"
                    range={{
                      start: 50000,
                      end: 500000,
                    }}
                    defaultValue={{
                      start: 50000,
                      end: 500000,
                    }}
                    rangeLabels={{
                      start: "50K",
                      end: "5L",
                    }}
                    showHistogram={false}
                    react={{
                      and: [
                        "result",
                        "PriceSensor",
                        "InstallmentSensor",
                        "SeatsSensor",
                        "DoorsSensor",
                        "TransmissionsSensor",
                      ],
                    }}
                    className="custom-range-slider"
                    innerClass={{
                      slider: "custom-range-slider-bar",
                    }}
                  />
                </Grid>
              </Typography>
            </AccordionDetails>
          </Accordion>
        </CardContent>
      </Card>

      <ButtonGroup
        className={classes.poppverbtns}
        variant="contained"
        aria-label="split button"
      >
        <Button className={classes.iconpoppverbtns} size="small">
          <MuseumIcon />
        </Button>
        <Button
          aria-describedby={id}
          variant="contained"
          onClick={handleManufacturerClick}
          /* className={
            btnActive === "changeBtnColor"
              ? classes.onchangetextbtns
              : classes.textbtns
          } */
           className={classes.textbtns}
        >
          Manufacturer
          <Chip label="x" className={classes.closebtn} />
        </Button>
      </ButtonGroup>

      <ButtonGroup
        variant="contained"
        className={classes.poppverbtns}
        aria-label="split button"
        style={{ marginTop: 20 }}
      >
        <Button className={classes.iconpoppverbtns} size="small">
          <AirlineSeatReclineExtraIcon />
        </Button>
        <Button
          aria-describedby={id}
          variant="contained"
          className={classes.textbtns}
          onClick={handleSeatsClick}
        >
          Seats
          <Chip label="x" className={classes.closebtn} />
        </Button>
      </ButtonGroup>

      <ButtonGroup
        variant="contained"
        className={classes.poppverbtns}
        aria-label="split button"
        style={{ marginTop: 20 }}
      >
        <Button className={classes.iconpoppverbtns} size="small">
          <AirportShuttleIcon />
        </Button>
        <Button
          aria-describedby={id}
          variant="contained"
          className={classes.textbtns}
          onClick={handleDoorsClick}
        >
          Doors
          <Chip label="x" className={classes.closebtn} />
        </Button>
      </ButtonGroup>

      <ButtonGroup
        variant="contained"
        className={classes.poppverbtns}
        aria-label="split button"
        style={{ marginTop: 20 }}
      >
        <Button className={classes.iconpoppverbtns} size="small">
          <TransformIcon />
        </Button>
        <Button
          aria-describedby={id}
          variant="contained"
          className={classes.textbtns}
          onClick={handleTransmissionClick}
        >
          Transmission
          <Chip label="x" className={classes.closebtn} />
        </Button>
      </ButtonGroup>

      <ButtonGroup
        variant="contained"
        className={classes.poppverbtns}
        aria-label="split button"
        style={{ marginTop: 20 }}
      >
        <Button className={classes.iconpoppverbtns} size="small">
          <LanguageIcon />
        </Button>
        <Button
          aria-describedby={id}
          variant="contained"
          className={classes.textbtns}
          onClick={handleCountryClick}
        >
          Country
          <Chip label="x" className={classes.closebtn} />
        </Button>
      </ButtonGroup>

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        onEnter={changeBtnColor}
        onExited={changeBackBtnColor}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <Typography className="typography">
              
          <MultiList
          className="MultiListPopOver"
            componentId="SeatsSensor"
            dataField={dataFieldValue}
            selectAllLabel ="Select All"
            showSearch={false}
          />
        </Typography>
      </Popover>

      {/* <MultiDropdownList
        componentId="ManufacturerSensor"
        dataField="model_data.manufacturer.name.keyword"
        size={100}
        title="Manufacturer"
        showCount={true}
        placeholder="Search Manufacture"
        showFilter={true}
        filterLabel="Manufacturer"
        URLParams={false}
        loader="Loading ..."
        react={{
          and: ["result", "PriceSensor", "SeatsSensor", "DoorsSensor"],
        }}
      />

      <MultiDropdownList
        componentId="SeatsSensor"
        dataField="model_data.seats"
        size={100}
        sortBy="count"
        title="Seats"
        showCount={true}
        placeholder="Search Seats"
        showFilter={true}
        filterLabel="Seats"
        URLParams={false}
        loader="Loading ..."
        react={{
          and: [
            "result",
            "PriceSensor",
            "DoorsSensor",
            "TransmissionsSensor",
            "ManufacturerSensor",
          ],
        }}
        showCheckbox={true}
      />

      <MultiDropdownList
        componentId="DoorsSensor"
        dataField="model_data.doors"
        size={100}
        sortBy="count"
        title="Doors"
        showCount={true}
        placeholder="Search Doors"
        showFilter={true}
        filterLabel="Doors"
        URLParams={false}
        loader="Loading ..."
        react={{
          and: [
            "result",
            "PriceSensor",
            "SeatsSensor",
            "TransmissionsSensor",
            "ManufacturerSensor",
          ],
        }}
        showCheckbox={true}
      />

      <MultiDropdownList
        componentId="TransmissionsSensor"
        dataField="model_data.transmission.keyword"
        size={100}
        title="Transmission"
        showCount={true}
        placeholder="Search Transmission"
        showFilter={true}
        filterLabel="Transmission"
        URLParams={false}
        loader="Loading ..."
        react={{
          and: [
            "result",
            "PriceSensor",
            "SeatsSensor",
            "DoorsSensor",
            "CountrySensor",
            "ManufacturerSensor",
          ],
        }}
      />

      <MultiDropdownList
        componentId="CountrySensor"
        dataField="model_data.country.name.keyword"
        size={100}
        title="Country"
        showCount={true}
        placeholder="Search Country"
        showFilter={true}
        filterLabel="Country"
        URLParams={false}
        loader="Loading ..."
        react={{
          and: [
            "result",
            "PriceSensor",
            "SeatsSensor",
            "DoorsSensor",
            "ManufacturerSensor",
          ],
        }}
      /> */}
    </div>
  );
};

export default Sidebar;

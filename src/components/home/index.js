//files-import
import Header from "../includes/Header";
import Banner from "./Banner";
import Search from "./Search";
const index = (props) => {
  return (
    <div className="content">
      <Header />
      <Banner />
      <Search />
    </div>
  );
};

export default index;

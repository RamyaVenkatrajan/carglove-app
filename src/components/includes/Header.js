import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import SaveAnalysisModal from "./SaveAnalysisModal";
import SignInModal from "../user/SignInModal";

export default function App() {
  const handleMenu = () => {
    console.log("header handle menu");
  };
  
  return (
    <div>
      <AppBar position="static" className="header-section">
        <Toolbar>
          <Typography variant="h6" className="header-title">
           <img src="./static/images/avatar/C1.png" alt="CarGlove"/>
          </Typography>

          <Button
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
          >
            My Analysis
          </Button>

          <Button>
            <SaveAnalysisModal />
          </Button>
          <Button>
            <SignInModal />
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

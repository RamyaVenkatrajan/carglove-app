import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import "../../App.css";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 32px !important",
  },
  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
}));

export default function SaveAnalysisModal() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    setOpen(false);
  };

  const body = (
    <div className="cardmodal">
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title">Save Analysis</h2>
        <Chip label="x" className={classes.closebtn} onClick={handleClick} />
        <p id="simple-modal-description">
          <TextField
            required
            id="outlined-required"
            label="Analysis Name"
            variant="outlined"
            className="modalfields"
          />

          <Button variant="contained" className={classes.textbtns}>
            Save Analysis
          </Button>
        </p>
      </div>
    </div>
  );

  return (
    <div>
      <button type="button" className="saveAnalysis-btn" onClick={handleOpen}>
        Save Analysis
      </button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}

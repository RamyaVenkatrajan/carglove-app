import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";

import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import "../../App.css";

import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";

import Checkbox from "@material-ui/core/Checkbox";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

import SignUpModal from "./SignUpModal.js";
//styles
function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 32px !important",
  },
  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    marginBottom: 10,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
  linktxt: {
    fontSize: 14,
    float: "right",
    color: "#ea001e",
    "&:hover": {
      color: "#ea001e",
    },
  },

  remember: {},
}));

export default function SignInModal() {
  //styles
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);

  //modal realted
  const [signInModalOpen, setSignInModalOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  //behaviour flags
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  //user attributes
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [rememberMe, setRememberMe] = useState(false);

  const [loading, setLoading] = useState(false);

  const requestOptions = {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    //headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  };
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = () => {
    setLoading(true);
    fetch(`http://localhost:3090/v1/auth/login`, requestOptions)
      .then((res) => res.json())
      .then((response) => {
        setLoading(false);
        if (response.status === "success") {
          setSignInModalOpen(false);
          setIsLoggedIn(false);
        }
        console.log("fetch cal response:" + response.status);
      })
      .catch((error) => console.log(error));
  };

  //changes may done here
  const handleDropDown = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    setIsLoggedIn(true);
    window.location.reload();
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseSignInModal = () => {
    setSignInModalOpen(false);
    //alert("");
  };

  const handleOpenModal = () => {
    setSignInModalOpen(true);
  };

  const handleSignInClick = () => {
    fetchData();
    setUsername("");
    setPassword("");  
  };

  const signInBody = (
    <div className="cardmodal">
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title">Sign In</h2>
        <Chip
          label="x"
          className={classes.closebtn}
          onClick={handleCloseSignInModal}
        />
        <p id="simple-modal-description">
          <TextField
            required
            id="outlined-required"
            label="Username"
            variant="outlined"
            className="modalfields"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />

          <FormControl className="modalfields1" variant="outlined">
            <InputLabel htmlFor="outlined-adornment-password">
              Password *
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              type={showPassword ? "text" : "password"}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    edge="end"
                    onClick={(e) => setShowPassword(!showPassword)}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
              labelWidth={90}
              onChange={(e) => setPassword(e.target.value)}
              value={password}
            />
          </FormControl>

          <FormControlLabel
            control={<Checkbox value="remember" />}
            label="Remember me"
            className={classes.remember}
            onClick={(e) => setRememberMe(!rememberMe)}
          />

          <Button
            variant="contained"
            className={classes.textbtns}
            onClick={handleSignInClick}
          >
            Sign In
          </Button>

          <Grid container>
            <Grid item xs>
              <Link href="#" className={classes.linktxt}>
                Forgot password?
              </Link>
            </Grid>
          </Grid>

          <Grid container style={{ justifyContent: "center", marginTop: 10 }}>
            <Grid item>
              <Link href="#" className={classes.linktxt}>
                <SignUpModal />
              </Link>
            </Grid>
          </Grid>
        </p>
      </div>
    </div>
  );

  let AuthButton;
  if (!isLoggedIn) {
    AuthButton = (
      <button type="button" className="signIn-btn" onClick={handleOpenModal}>
        Sign In
      </button>
    );
  } else {
    AuthButton = (
      <div>
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleMenu}
          color="inherit"
          style={{ color: "#FFF", fontWeight: 500, fontSize: 15 }}
        >
          Welcome {username}
          <ArrowDropDownIcon size="small" />
        </Button>

        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          keepMounted
          transformOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={open}
          onClose={handleDropDown}
          style={{ top: 40 }}
        >
          <MenuItem onClick={handleLogout}>Logout</MenuItem>
        </Menu>
      </div>
    );
  }

  return (
    <div>
      {AuthButton}
      <Modal
        open={signInModalOpen}
        onClose={handleCloseSignInModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {signInBody}
      </Modal>
    </div>
  );
}

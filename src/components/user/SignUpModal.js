import React, { useState, useRef } from "react";
import Axios from "axios";
import ReCAPTCHA from "react-google-recaptcha";

import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import "../../App.css";

import Link from "@material-ui/core/Link";
import { Scrollbars } from "react-custom-scrollbars";
import Grid from "@material-ui/core/Grid";
import SignInModal from "./SignInModal";

import { isEmail, isAlpha, isAlphanumeric, isStrongPassword } from "validator";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 32px !important",
  },
  textbtns: {
    textTransform: "capitalize",
    backgroundImage: "linear-gradient(90deg, #2e0b35, #9c1233)",
    borderColor: "#9c1233",
    width: "100%",
    borderRight: 0,
    color: "#fff",
    "&:hover": {
      color: "#fff",
    },
  },
  closebtn: {
    width: 20,
    height: 20,
    position: "absolute",
    right: -6,
    top: -6,
    color: "#fff",
    background: "#dc3545",
    fontSize: 12,
  },
  linktxt1: {
    fontSize: 14,
    color: "#ea001e",
    "&:hover": {
      color: "#ea001e",
    },
  },
}));

export default function SignUpModal() {
  const form = useRef();
  const classes = useStyles();

  //user variables
  const [firstName, setFirstName] = useState("udhaay");
  const [lastName, setLastName] = useState("ora");
  const [title, setTitle] = useState("mr");
  const [username, setUsername] = useState("user1");
  const [email, setEmail] = useState("email@test.com");
  const [password, setPassword] = useState("Test@1234");
  const [mobile, setMobile] = useState("9843729432");
  const [landline, setLandline] = useState("894735");
  const [address, setAddress] = useState("adress");
  const [successful, setSuccessful] = useState(false);
  const [message, setMessage] = useState("");
  const [captchaResponse, setCaptchaResponse] = useState("");

  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [signUpModalOpen, setSignUpModalOpen] = React.useState(false);
  const [signInModalOpen, setSignInModalOpen] = React.useState(false);

  const handleOpen = () => {
    setSignUpModalOpen(true);
  };

  const handleCloseSignUpModal = () => {
    setSignUpModalOpen(false);
  };

  const handleOpenSignInModal = () => {
    setSignInModalOpen(true);
  };

  const onChangeCaptcha = (value) => {
    setCaptchaResponse(value);
  };

  //onform submit
  const handleRegister = async (e) => {
    e.preventDefault();
    console.log("inside handleRegister", captchaResponse);
    setMessage("");
    setSuccessful(false);

    Axios.post("http://localhost:3090/v1/auth/register", {
      username: username,
      password: password,
      email: email,
      firstName: firstName,
      lastName: lastName,
      title: title,
      mobile: mobile,
      landline: landline,
      address: address,
      captchaResponse: captchaResponse,
    }).then((response) => {
      console.log("reg-response", response);
    });
  };

  //required
  const required = (value) => {
    if (!value) {
      return (
        <div className="alert alert-danger" role="alert">
          This field is required!
        </div>
      );
    }
  };

  //validations
  const validateUsername = (value) => {
    console.log("inisde validateUsername");

    if (value.length < 3 && value.length > 10) {
      if (!isAlphanumeric(value)) {
        return (
          <div className="alert alert-danger" role="alert">
            Invalid username. Username length should be between 3-10 charecters
            and contains letters and numbers.
          </div>
        );
      }
    } else {
      return (
        <div className="alert alert-danger" role="alert">
          Invalid username. Username length should be between 3-10 charecters
          and contains letters and numbers.
        </div>
      );
    }
  };

  const validateName = (value) => {
    console.log("inisde validateUsername");

    if (value.length < 3 && value.length > 25) {
      if (!isAlpha(value)) {
        return (
          <div className="alert alert-danger" role="alert">
            Name length should be between 3-25 charecters and should contains
            letters only.
          </div>
        );
      }
    } else {
      return (
        <div className="alert alert-danger" role="alert">
          Name length should be between 3-25 charecters and should contains
          letters only.
        </div>
      );
    }
  };

  const validateEmail = (value) => {
    if (!isEmail(value)) {
      return (
        <div className="alert alert-danger" role="alert">
          This is not a valid email.
        </div>
      );
    }
  };

  const validatePassword = (value) => {
    if (value.length < 6 || value.length > 15) {
      //if (!isStrongPassword(value)) {
      return (
        <div className="alert alert-danger" role="alert">
          The password must be between 6 and 15 characters.
        </div>
      );
      // }
    }
  };

  const validateConfirmPassword = (value) => {
    if (!password === value) {
      //if (!isStrongPassword(value)) {
      return (
        <div className="alert alert-danger" role="alert">
          Confirm password should match.
        </div>
      );
      // }
    }
  };

  const signUpBody = (
    <div className="cardmodal">
      {!successful && (
        <div style={modalStyle} className={classes.paper}>
          <h2 id="simple-modal-title">Sign Up</h2>
          <Chip
            label="x"
            className={classes.closebtn}
            onClick={handleCloseSignUpModal}
          />

          <Scrollbars style={{ height: 450 }}>
            <p id="simple-modal-description" class="signup">
              <TextField
                required
                id="outlined-required"
                label="First Name"
                variant="outlined"
                className="modalfields"
                type="String"
                style={{ marginTop: 10 }}
                onChange={(e) => setFirstName(e.target.value)}
                validations={[required, validateName]}
              />

              <TextField
                required
                id="outlined-required"
                label="Last Name"
                variant="outlined"
                className="modalfields"
                type="String"
                onChange={(e) => setLastName(e.target.value)}
                validations={[required, validateName]}
              />

              <TextField
                required
                id="outlined-required"
                label="Title"
                variant="outlined"
                className="modalfields"
                type="String"
                onChange={(e) => setTitle(e.target.value)}
              />

              <TextField
                required
                id="outlined-required"
                label="User Name"
                variant="outlined"
                className="modalfields"
                type="String"
                onChange={(e) => setUsername(e.target.value)}
                validations={[required, validateUsername]}
              />

              <TextField
                required
                id="outlined-required"
                label="E-mail"
                p
                variant="outlined"
                className="modalfields"
                type="email"
                onChange={(e) => setEmail(e.target.value)}
                validations={[required, validateEmail]}
              />

              <TextField
                required
                id="outlined-required"
                label="Password"
                variant="outlined"
                className="modalfields"
                type="password"
                onChange={(e) => setPassword(e.target.value)}
                validations={[required, validatePassword]}
              />

              <TextField
                required
                id="outlined-required"
                label="Confirm Password"
                variant="outlined"
                className="modalfields"
                type="password"
                validations={[required, validateConfirmPassword]}
              />

              <TextField
                required
                id="outlined-required"
                label="Mobile no"
                variant="outlined"
                className="modalfields"
                type="tel"
                onChange={(e) => setMobile(e.target.value)}
              />

              <TextField
                required
                id="outlined-required"
                label="Landline no"
                variant="outlined"
                className="modalfields"
                type="tel"
                onChange={(e) => setLandline(e.target.value)}
              />

              <TextField
                required
                id="outlined-required"
                label="Address"
                variant="outlined"
                className="modalfields"
                multiline
                rows={2}
                type="String"
                onChange={(e) => setAddress(e.target.value)}
              />

              <ReCAPTCHA
                sitekey="6LdJ-04aAAAAADMId22n9nR6xKNFj85xuP5a81Sm"
                onChange={onChangeCaptcha}
                size="normal"
              />
            </p>
          </Scrollbars>

          {/*    onloadCallback={onLoadRecaptcha}
                verifyCallback={verifyCallback} */}
          <Button
            variant="contained"
            className={classes.textbtns}
            onClick={handleRegister}
            id="action"
          >
            Sign Up{" "}
          </Button>

          <Grid container style={{ justifyContent: "center", marginTop: 10 }}>
            <Grid item>
              <Link
                href="#"
                className={classes.linktxt1}
                onClick={handleOpenSignInModal}
              >
                {"Already have an account? Sign In"}{" "}
                {signInModalOpen ? <SignInModal /> : null}
              </Link>
            </Grid>
          </Grid>
        </div>
      )}
      {message && (
        <div className="form-group">
          <div
            className={
              successful ? "alert alert-success" : "alert alert-danger"
            }
            role="alert"
          >
            {message}
          </div>
        </div>
      )}
    </div>
  );
  return (
    <div>
      <Link href="#" className={classes.linktxt1} onClick={handleOpen}>
        {"Don't have an account? Sign Up"}
      </Link>
      <Modal
        open={signUpModalOpen}
        onClose={handleCloseSignUpModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {signUpBody}
      </Modal>
    </div>
  );
}
